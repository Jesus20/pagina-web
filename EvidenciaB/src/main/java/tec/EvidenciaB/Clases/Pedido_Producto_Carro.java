package tec.EvidenciaB.Clases;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class Pedido_Producto_Carro {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int id;
    
    private int cantidad;
    private float precio_unitario;
    private float total;
    private int producto_id;
    private int pedido_carro_id;
    
    public Pedido_Producto_Carro () {
    }
    
    /*  Getters & Setters   */
    public int getId() {
        return id;
    }
    public void setId(int id) {
        this.id = id;
    }
    public int getCantidad() {
        return cantidad;
    }
    public void setCantidad(int cantidad) {
        this.cantidad = cantidad;
    }
    public float getPrecio_unitario() {
        return precio_unitario;
    }
    public void setPrecio_unitario(float precio_unitario) {
        this.precio_unitario = precio_unitario;
    }
    public float getTotal() {
        return total;
    }
    public void setTotal(float total) {
        this.total = total;
    }
    public int getProducto_id() {
        return producto_id;
    }
    public void setProducto_id(int producto_id) {
        this.producto_id = producto_id;
    }
    public int getPedido_Carro_id() {
        return pedido_carro_id;
    }
    public void setPedido_Carro_id(int pedido_carro_id) {
        this.pedido_carro_id = pedido_carro_id;
    }
    
}