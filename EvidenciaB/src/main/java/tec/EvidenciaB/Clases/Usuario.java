package tec.EvidenciaB.Clases;

import java.math.BigInteger;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import java.util.Date;

@Entity
public class Usuario {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int id;
    
    private String nombres;
    private String apellidos;
    private String correo;
    private String contrasenia;
    private int telefono;
    private String direccion1;
    private String direccion2;
    private String ciudad;
    private String estado;
    private int codigo_postal;
    private String tipo_tarjeta;
    private BigInteger numero_tarjeta;
    private Date fecha_expiracion;
    private int cvv;
    private String token;
    
    /*  Controladores   */
    public Usuario() {
    }
    public Usuario(String correo, String contrasenia) {
        this.correo = correo;
        this.contrasenia = contrasenia;
    }
    public Usuario(int id, String correo, String contrasenia) {
        this.id = id;
        this.correo = correo;
        this.contrasenia = contrasenia;
    }
    
    /*  Getters & Setters   */
    public int getId() {
        return id;
    }
    public void setId(int id) {
        this.id = id;
    }
    public String getNombres() {
        return nombres;
    }
    public void setNombres(String nombres) {
        this.nombres = nombres;
    }
    public String getApellidos() {
        return apellidos;
    }
    public void setApellidos(String apellidos) {
        this.apellidos = apellidos;
    }
    public String getCorreo() {
        return correo;
    }
    public void setCorreo(String correo) {
        this.correo = correo;
    }
    public String getContrasenia() {
        return contrasenia;
    }
    public void setContrasenia(String contrasenia) {
        this.contrasenia = contrasenia;
    }
    public int getTelefono() {
        return telefono;
    }
    public void setTelefono(int telefono) {
        this.telefono = telefono;
    }
    public String getDireccion1() {
        return direccion1;
    }
    public void setDireccion1(String direccion1) {
        this.direccion1 = direccion1;
    }
    public String getDireccion2() {
        return direccion2;
    }
    public void setDireccion2(String direccion2) {
        this.direccion2 = direccion2;
    }
    public String getCiudad() {
        return ciudad;
    }
    public void setCiudad(String ciudad) {
        this.ciudad = ciudad;
    }
    public String getEstado() {
        return estado;
    }
    public void setEstado(String estado) {
        this.estado = estado;
    }
    public int getCodigo_postal() {
        return codigo_postal;
    }
    public void setCodigo_postal(int codigo_postal) {
        this.codigo_postal = codigo_postal;
    }
    public String getTipo_tarjeta() {
        return tipo_tarjeta;
    }
    public void setTipo_tarjeta(String tipo_tarjeta) {
        this.tipo_tarjeta = tipo_tarjeta;
    }
    public BigInteger getNumero_tarjeta() {
        return numero_tarjeta;
    }
    public void setNumero_tarjeta(BigInteger numero_tarjeta) {
        this.numero_tarjeta = numero_tarjeta;
    }
    public Date getFecha_expiracion() {
        return fecha_expiracion;
    }
    public void setFecha_expiracion(Date fecha_expiracion) {
        this.fecha_expiracion = fecha_expiracion;
    }
    public int getCvv() {
        return cvv;
    }
    public void setCvv(int cvv) {
        this.cvv = cvv;
    }
    public String getToken() {
        return token;
    }
    public void setToken(String token) {
        this.token = token;
    }
    
}