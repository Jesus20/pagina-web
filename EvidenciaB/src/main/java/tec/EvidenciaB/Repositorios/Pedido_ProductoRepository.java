package tec.EvidenciaB.Repositorios;

import java.util.List;
import org.springframework.data.repository.CrudRepository;

import tec.EvidenciaB.Clases.Pedido_Producto;

public interface Pedido_ProductoRepository extends CrudRepository<Pedido_Producto, Integer>  {
    
    List<Pedido_Producto> findAll();
    
}