package tec.EvidenciaB.Repositorios;

import java.util.List;
import org.springframework.data.repository.CrudRepository;

import tec.EvidenciaB.Clases.Categoria;

public interface CategoriaRepository extends CrudRepository<Categoria, Integer> {
    
    List<Categoria> findAll();
    
}