package tec.EvidenciaB.Repositorios;

import java.util.List;
import org.springframework.data.repository.CrudRepository;

import tec.EvidenciaB.Clases.Pedido_Carro;

public interface Pedido_CarroRepository extends CrudRepository<Pedido_Carro, Integer>  {
    
    List<Pedido_Carro> findAll();
    
}