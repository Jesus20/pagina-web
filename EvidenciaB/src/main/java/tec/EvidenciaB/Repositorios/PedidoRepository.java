package tec.EvidenciaB.Repositorios;

import org.springframework.data.repository.CrudRepository;

import tec.EvidenciaB.Clases.Pedido;

public interface PedidoRepository extends CrudRepository<Pedido, Integer> {
    
}