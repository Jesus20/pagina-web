package tec.EvidenciaB.Repositorios;

import org.springframework.data.repository.CrudRepository;

import tec.EvidenciaB.Clases.Usuario;

public interface UsuarioRepository extends CrudRepository<Usuario, Integer> {
    
    Usuario findByCorreo(String correo);
    
}