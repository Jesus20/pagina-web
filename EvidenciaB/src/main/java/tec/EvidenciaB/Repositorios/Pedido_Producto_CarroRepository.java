package tec.EvidenciaB.Repositorios;

import java.util.List;
import org.springframework.data.repository.CrudRepository;

import tec.EvidenciaB.Clases.Pedido_Producto_Carro;

public interface Pedido_Producto_CarroRepository extends CrudRepository<Pedido_Producto_Carro, Integer> {
    
    List<Pedido_Producto_Carro> findAll();
    
}