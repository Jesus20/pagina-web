package tec.EvidenciaB.Repositorios;

import java.util.List;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import tec.EvidenciaB.Clases.Producto_Categoria;

public interface Producto_CategoriaRepository extends CrudRepository<Producto_Categoria, Integer> {
    
    List<Producto_Categoria> findAll();
    
    @Query("select p.id, p.nombre, p.caracteristicas, p.marca, p.imagen, p.precio, count(*) \n"
            + "from Producto_Categoria pc, Producto p, Categoria c \n"
            + "where pc.producto.id = p.id  \n"
            + "and pc.categoria.id = 1 \n"
            + "group by p.id having count(*) > 1 \n"
            + "order by p.id")
    public List<Object> categoriaCafe0();
    
    @Query("select p.id, p.nombre, p.caracteristicas, p.marca, p.imagen, p.precio, count(*) \n"
            + "from Producto_Categoria pc, Producto p, Categoria c \n"
            + "where pc.producto.id = p.id  \n"
            + "and pc.categoria.id = 2 \n"
            + "group by p.id having count(*) > 1 \n"
            + "order by p.id")
    public List<Object> categoriaCafe1();
    
    @Query("select p.id, p.nombre, p.caracteristicas, p.marca, p.imagen, p.precio, count(*) \n"
            + "from Producto_Categoria pc, Producto p, Categoria c \n"
            + "where pc.producto.id = p.id  \n"
            + "and pc.categoria.id = 3 \n"
            + "group by p.id having count(*) > 1 \n"
            + "order by p.id")
    public List<Object> categoriaCafe2();
    
    @Query("select p.id, p.nombre, p.caracteristicas, p.marca, p.imagen, p.precio, count(*) \n"
            + "from Producto_Categoria pc, Producto p, Categoria c \n"
            + "where pc.producto.id = p.id  \n"
            + "and pc.categoria.id = 4 \n"
            + "group by p.id having count(*) > 1 \n"
            + "order by p.id")
    public List<Object> categoriaCafeteras();
    
    @Query("select p.id, p.nombre, p.caracteristicas, p.marca, p.imagen, p.precio, count(*) \n"
            + "from Producto_Categoria pc, Producto p, Categoria c \n"
            + "where pc.producto.id = p.id  \n"
            + "and pc.categoria.id = 5 \n"
            + "group by p.id having count(*) > 1 \n"
            + "order by p.id")
    public List<Object> categoriaAccesorios();
    
}