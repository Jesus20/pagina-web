package tec.EvidenciaB.Repositorios;

import java.util.List;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import tec.EvidenciaB.Clases.Producto;

public interface ProductoRepository extends CrudRepository<Producto, Integer> {
    
    List<Producto> findAll();
    
    @Query("select p.id, p.nombre, p.caracteristicas, p.marca, p.imagen, p.precio from Producto p \n"
            + "where p.nombre like %:buscar% \n"
            + "order by p.id")
    public List<Object> find(@Param("buscar") String buscar);
    
}