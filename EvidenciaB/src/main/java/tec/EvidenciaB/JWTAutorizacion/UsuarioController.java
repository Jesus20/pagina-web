package tec.EvidenciaB.JWTAutorizacion;

import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.beans.factory.annotation.Autowired;

import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;

import tec.EvidenciaB.Repositorios.UsuarioRepository;
import tec.EvidenciaB.Clases.Usuario;

@RestController
public class UsuarioController {
    
    @Autowired
    UsuarioRepository usuarioRepository;
    
    @PostMapping("/user")
    public Usuario login(@RequestParam("correo") String correo, @RequestParam("contrasenia") String contrasenia) {
        
        try {
            
            if(usuarioRepository.findByCorreo(correo).getCorreo().equals(correo)) {
                
                if(usuarioRepository.findByCorreo(correo).getCorreo().equals(contrasenia)) {
                    String token = getJWTToken(correo);
                    Usuario user = new Usuario();
                    user.setId(usuarioRepository.findByCorreo(correo).getId());
                    user.setCorreo(correo);
                    user.setToken(token);
                    return user;
                } else {
                    Usuario user = new Usuario();
                    user.setCorreo(correo);
                    user.setToken("Error en el correo o contraseña");	
                    return user;
                }
                
            } else {
                Usuario user = new Usuario();
                user.setCorreo(correo);
                user.setToken("Error en el correo o contraseña");	
                return user;
            }
            
        } catch(Exception exception) {
            Usuario user = new Usuario();
            user.setCorreo(correo);
            user.setToken("Error en el correo o contraseña");	
            return user;
        }
        
    }
    
    private String getJWTToken(String username) {
        String secretKey = "mySecretKey";
	List<GrantedAuthority> grantedAuthorities = AuthorityUtils
            .commaSeparatedStringToAuthorityList("ROLE_USER");
	String token = Jwts
            .builder()
            .setSubject(username)
            .claim("authorities", grantedAuthorities.stream()
                .map(GrantedAuthority::getAuthority)
                .collect(Collectors.toList()))
            .setIssuedAt(new Date(System.currentTimeMillis()))
            .setExpiration(new Date(System.currentTimeMillis() + 600000))
            .signWith(SignatureAlgorithm.HS512, secretKey.getBytes())
            .compact();
        return "Bearer " + token;
    }
    
}