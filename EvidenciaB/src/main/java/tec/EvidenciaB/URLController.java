package tec.EvidenciaB;

import java.lang.reflect.Array;
import java.util.List;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.ResponseBody;

import tec.EvidenciaB.Repositorios.UsuarioRepository;
import tec.EvidenciaB.Repositorios.ProductoRepository;
import tec.EvidenciaB.Repositorios.CategoriaRepository;
import tec.EvidenciaB.Repositorios.PedidoRepository;
import tec.EvidenciaB.Repositorios.Pedido_CarroRepository;
import tec.EvidenciaB.Repositorios.Producto_CategoriaRepository;
import tec.EvidenciaB.Repositorios.Pedido_ProductoRepository;
import tec.EvidenciaB.Repositorios.Pedido_Producto_CarroRepository;
import tec.EvidenciaB.Clases.Usuario;
import tec.EvidenciaB.Clases.Producto;
import tec.EvidenciaB.Clases.Categoria;
import tec.EvidenciaB.Clases.Pedido;
import tec.EvidenciaB.Clases.Pedido_Carro;
import tec.EvidenciaB.Clases.Producto_Categoria;
import tec.EvidenciaB.Clases.Pedido_Producto;
import tec.EvidenciaB.Clases.Pedido_Producto_Carro;

@RestController
public class URLController {
    
    @Autowired
    UsuarioRepository usuarioRepository;
    
    @Autowired
    ProductoRepository productoRepository;
    
    @Autowired
    CategoriaRepository categoriaRepository;
    
    @Autowired
    PedidoRepository pedidoRepository;
    
    @Autowired
    Pedido_CarroRepository pedido_carroRepository;
    
    @Autowired
    Producto_CategoriaRepository producto_categoriaRepository;
    
    @Autowired
    Pedido_ProductoRepository pedido_productoRepository;
    
    @Autowired
    Pedido_Producto_CarroRepository pedido_producto_carroRepository;
    
    @GetMapping("/hello")
    public String HelloWorld() {
        return "Hello World!!";
    }
    
    @PostMapping("/registrar_usuario")
    public @ResponseBody String Registrar_Usuario(@RequestBody Usuario user) {
        Usuario newUser = new Usuario();
        newUser.setNombres(user.getNombres());
        newUser.setApellidos(user.getApellidos());
        newUser.setCorreo(user.getCorreo());
        newUser.setContrasenia(user.getContrasenia());
        newUser.setTelefono(user.getTelefono());
        newUser.setDireccion1(user.getDireccion1());
        newUser.setDireccion2(user.getDireccion2());
        newUser.setCiudad(user.getCiudad());
        newUser.setEstado(user.getEstado());
        newUser.setCodigo_postal(user.getCodigo_postal());
        newUser.setTipo_tarjeta(user.getTipo_tarjeta());
        newUser.setNumero_tarjeta(user.getNumero_tarjeta());
        newUser.setFecha_expiracion(user.getFecha_expiracion());
        newUser.setCvv(user.getCvv());
        usuarioRepository.save(newUser);
        return "SUCCESS";
    }
    
    @PostMapping("/producto_select")
    public @ResponseBody Producto Product_Select(@RequestBody Producto product) {
        Producto producto_select = new Producto();
        producto_select = productoRepository.findById(product.getId()).get();
        return producto_select;
    }
    
    @PostMapping("/usuario")
    public @ResponseBody Usuario Usuario(@RequestBody Usuario user) {
        Usuario searchUser = new Usuario();
        
        if(usuarioRepository.findByCorreo(user.getCorreo()).getCorreo().equals(user.getCorreo())) {
            if(usuarioRepository.findByCorreo(user.getCorreo()).getContrasenia().equals(user.getContrasenia())) {
                searchUser = usuarioRepository.findByCorreo(user.getCorreo());
            } else {
                return searchUser;
            }
        } else {
            return searchUser;
        }
        
        return searchUser;
    }
    
    @PostMapping("/usuario_secion")
    public @ResponseBody Usuario Usuario_Secion(@RequestBody Usuario user) {
        Usuario usuario = new Usuario(); 
        usuario = usuarioRepository.findById(user.getId()).get();
        return usuario;
    }
    
    @PostMapping("/ingresar_producto")
    public @ResponseBody String Insert_producto(@RequestBody Pedido_Producto_Carro producto) {        
        Pedido_Producto_Carro producto_insert = new Pedido_Producto_Carro();
        producto_insert.setCantidad(producto.getCantidad());
        producto_insert.setPedido_Carro_id(producto.getPedido_Carro_id());
        producto_insert.setPrecio_unitario(producto.getPrecio_unitario());
        producto_insert.setProducto_id(producto.getProducto_id());
        producto_insert.setTotal(producto.getTotal());
        pedido_producto_carroRepository.save(producto_insert);
        
        int subtotal = 0;
        
        for(int i = 0; i < pedido_producto_carroRepository.count(); i++) {
            if(pedido_producto_carroRepository.findById(i).get().getPedido_Carro_id() == producto.getPedido_Carro_id()) {
                subtotal += pedido_producto_carroRepository.findById(i).get().getTotal();
            }
        }
        
        Pedido_Carro producto_carro = new Pedido_Carro(); 
        producto_carro.setId(producto.getId());
        producto_carro.setIva(subtotal/10);
        producto_carro.setSubtotal(subtotal);
        producto_carro.setTotal(producto_carro.getIva() + producto_carro.getSubtotal());
        producto_carro.setUsuario_id(1);
        pedido_carroRepository.save(producto_carro);
        
        return "SUCCESS";
    }
    
    @GetMapping("/productos")
    public @ResponseBody List<Producto> Productos() {
        List<Producto> productos = productoRepository.findAll();
        return productos;
    }
    
    @GetMapping("/carro")
    public @ResponseBody Object[] Carro () {
        Object[] pedido_carro = {pedido_carroRepository.findAll(), pedido_producto_carroRepository.findAll()};
        return pedido_carro;
    }
    
    @GetMapping("/pedidos")
    public @ResponseBody Object[] Pedido () {
        Object[] pedido = {pedidoRepository.findAll(), pedido_productoRepository.findAll()};
        return pedido;
    }
    
    @GetMapping("/buscador")
    public @ResponseBody List<Object> Buscador(@RequestParam(value="buscar") String buscar) {
        List<Object> producto_buscado = productoRepository.find(buscar);
        return producto_buscado;
    }
    
    @GetMapping("/cafes")
    public @ResponseBody List<Object> Cafes () {
        List<Object> producto_buscado = producto_categoriaRepository.categoriaCafe0();
        return producto_buscado;
    }
    
    @GetMapping("/cafes_granos")
    public @ResponseBody List<Object> Cafes1 () {
        List<Object> producto_buscado = producto_categoriaRepository.categoriaCafe1();
        return producto_buscado;
    }
    
    @GetMapping("/cafes_capsulas")
    public @ResponseBody List<Object> Cafes2 () {
        List<Object> producto_buscado = producto_categoriaRepository.categoriaCafe2();
        return producto_buscado;
    }
    
    @GetMapping("/cafeteras")
    public @ResponseBody List<Object> Cafeteras () {
        List<Object> producto_buscado = producto_categoriaRepository.categoriaCafeteras();
        return producto_buscado;
    }
    
    @GetMapping("/accesorios")
    public @ResponseBody List<Object> Accesorios () {
        List<Object> producto_buscado = producto_categoriaRepository.categoriaAccesorios();
        return producto_buscado;
    }
    
    /*
    @GetMapping("/prueba")
    public @ResponseBody List<Pedido_Producto_Carro> Prueba() {
        List<Pedido_Producto_Carro> prueba = pedido_producto_carroRepository.findAll();
        return prueba;
    }
    */
   
}