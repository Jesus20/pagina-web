$(document).ready(function() {
    event.preventDefault();
    
    $.ajax({
        url: "http://localhost:8081/productos",
        type: "GET",
        // data: JSON.stringify(data),
        contentType:"application/json",
    }).then(function(data) {
        
        for(i = 0; i < data.length; i++) {
            var tabla ="<div class='container'> \n\
                            <div class='row'> \n\
                                <div class='col'> \n\
                                    <div class='card' style='width: 18rem;'> \n\
                                        <img src='Imagenes/"+data[i].imagen+"' width='40' height='300' class='card-img-top' alt='...'> \n\
                                        <div class='card-body'> \n\
                                            <h5 class='card-title'>"
                                                +data[i].nombre+ 
                                           "</h5> \n\
                                            <p class='card-text'> \n\
                                                $" +data[i].precio+ 
                                           "</p> \n\
                                            <a href='http://localhost:8080/EvidenciaF/producto.html?id="+(i+1)+"' class='btn btn-primary'> \n\
                                                Ir a artículo \n\
                                            </a> \n\
                                        </div> \n\
                                    </div> \n\
                                </div> \n\
                            </div> \n\
                        </div>";
            
            if(i == 0) {
                $("#producto1").html(tabla);
            } else {
                if(i == 2) {
                    $("#producto2").html(tabla);
                } else {
                    if(i == 7) {
                        $("#producto3").html(tabla);
                    } else {
                        if(i == 10) {
                            $("#producto4").html(tabla);
                        } else {
                            if(i == 13) {
                                $("#producto5").html(tabla);
                            } else {
                                if(i == 16) {
                                    $("#producto6").html(tabla);
                                }
                            }
                        }
                    }
                }
            }
            
        }
        
    });
    
    $.ajax({
        url: "http://localhost:8081/carro",
        type: "GET",
        // data: JSON.stringify(data),
        contentType:"application/json",
    }).then(function(data) {
        $("#itemsTotal1").html(data[1].length);
    });
    
});