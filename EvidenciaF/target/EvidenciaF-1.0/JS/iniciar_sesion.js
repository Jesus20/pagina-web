$(document).ready(function() {
    event.preventDefault();
    
    $("#registro").click(function(event) {
        event.preventDefault();
        
        var data = {
            correo : document.getElementById("inputEmail").value,
            contrasenia : document.getElementById("inputPassword").value
        }
        
        $.ajax({ 
            url: "http://localhost:8081/usuario",
            type: "POST",
            data: JSON.stringify(data),
            contentType:"application/json",
        }).then(function(data) {

            if(data.id != 0) {
                localStorage.setItem("user_id", data.id);
                location.href = "http://localhost:8080/EvidenciaF/secion_iniciada.html";
            } else {
                alert("Error al ingresar correo o contraseña!!");
            }

        }).fail(function(error) {
            alert("Error al ingresar correo o contraseña!!");
        });
        
    });
    
});