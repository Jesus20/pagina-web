$(document).ready(function() {
    
    $("#crear_sesion").click(function(event) {
        event.preventDefault();
        var nombres_inp = document.getElementById("nombres").value;
        var apellidos_inp = document.getElementById("apellidos").value;
        var correo_inp = document.getElementById("correo").value;
        var cont1 = document.getElementById("contrasenia1").value;
        var cont2 = document.getElementById("contrasenia2").value;
        var contrasenia_inp;
        var telefono_inp = document.getElementById("telefono").value;
        var direccion1_inp = document.getElementById("direccion1").value;
        var direccion2_inp = document.getElementById("direccion2").value;
        var ciudad_inp = document.getElementById("ciudades").value;
        var estado_inp = document.getElementById("estado").value;
        var codigo_postal_inp = document.getElementById("codigo_postal").value;
        var tipo_tarjeta_inp = document.getElementById("metodo_pago").value;
        var numero_tarjeta_inp = document.getElementById("tarjeta_numero").value;
        var fecha_expiracion_inp = document.getElementById("fecha_expiracion").value;
        var cvv_inp = document.getElementById("Cvv").value;
        
        if(cont1 == cont2) {
            contrasenia_inp = cont2; 
            
            var data = {
                nombres : nombres_inp,
                apellidos : nombres_inp,
                correo : correo_inp,
                contrasenia : contrasenia_inp,
                telefono : telefono_inp,
                direccion1 : direccion1_inp,
                direccion2 : direccion2_inp,
                ciudad : ciudad_inp,
                estado : estado_inp,
                codigo_postal : codigo_postal_inp,
                tipo_tarjeta : tipo_tarjeta_inp,
                numero_tarjeta : numero_tarjeta_inp,
                fecha_expiracion : fecha_expiracion_inp,
                cvv : cvv_inp
            };

            $.ajax({
                url: "http://localhost:8081/registrar_usuario",
                type: "POST",
                data: JSON.stringify(data),
                contentType:"application/json"
            }).then(function(data) {
                alert("Usuario registrado!!");
                location.href = "http://localhost:8080/EvidenciaF/";
            }).fail(function(error) {
                alert("Error al ingresar un dato!!");
            });
            
        } else {
            alert("Las contraseñas no coinciden!!");
        }
        
    });
    
});