$(document).ready(function() {
    event.preventDefault();
    
    var id = window.location.search.substr(4);
    
    var data = {
        id : id
    }
    
    $.ajax({ 
        url: "http://localhost:8081/producto_select",
        type: "POST",
        data: JSON.stringify(data),
        contentType:"application/json",
    }).then(function(data) {
        var id = data.id;
        var nombre = data.nombre;
        var caracteristicas = data.caracteristicas;
        var marca = data.marca;
        var imagen = data.imagen;
        var precio = data.precio;
        var cantidad = 0;
        var precio_total = 0;
        var nombre_html = "<h2 class='text-center' style='color:#FFFFFF'>" +nombre+ "</h2>"
        var imagen_html = "<img src='Imagenes/" +imagen+ "' width='400' height='400' alt=''/>";
        var caracteristicas_html = "<h5 style='color:#FFFFFF'> " +caracteristicas+ " </h5>";
        var precio_html = "<h5> Precio del producto: $" +precio+ " </h5>";
        $("#nombre_producto").html(nombre_html);
        $("#imagen_producto").html(imagen_html);
        $("#caracteristicas_producto").html(caracteristicas_html);
        $("#precio_producto").html(precio_html);
        $("#precio_total").html(0);
        
        $("#cantidad_producto").change(function(){
            cantidad = document.getElementById("cantidad_producto").value;
            precio_total = precio * cantidad;
            $("#precio_total").html(precio_total);
        });
        
        $("#ingresar_producto").click(function(event) {
            var precio_unitario = ((precio + precio_total) / cantidad);
            var subtotal = 0;
            
            var data = {
                cantidad : cantidad,
                precio_unitario : precio_unitario,
                total : precio_total,
                producto_id : id,
                pedido_carro_id : 1
            }
            
            $.ajax({ 
                url: "http://localhost:8081/ingresar_producto",
                type: "POST",
                data: JSON.stringify(data),
                contentType:"application/json",
            }).then(function(data) {
                console.log(data);
            }).fail(function(error) {
                console.log(error);
            });
            
            location.href = "http://localhost:8080/EvidenciaF/";
        });
        
    });
    
    $.ajax({
        url: "http://localhost:8081/carro",
        type: "GET",
        // data: JSON.stringify(data),
        contentType:"application/json",
    }).then(function(data) {
        $("#itemsTotal1").html(data[1].length);
    });
    
});