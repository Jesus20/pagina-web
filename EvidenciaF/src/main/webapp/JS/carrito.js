$(document).ready(function() {
    event.preventDefault();
    
    $.ajax({
        url: "http://localhost:8081/carro",
        type: "GET",
        // data: JSON.stringify(data),
        contentType:"application/json",
    }).then(function(data) {
        
        for(i = 0; i < data[1].length; i++) {
            var productos_insert = "<div class='order-md-2 mb-4'> \n\
                                        <ul class='list-group mb-3'> \n\
                                            <li class='list-group-item d-flex justify-content-between lh-condensed'> \n\
                                                <div> \n\
                                                    <h6 class='my-0'> \n\
                                                        Producto ID : "+data[1][i].producto_id+" \n\
                                                    </h6> \n\
                                                    <pre></pre> \n\
                                                    <h6> \n\
                                                        Cantidad: "+data[1][i].cantidad+" \n\
                                                    </h6> \n\
                                                </div> \n\
                                                <p> \n\
                                                    Precio: $"+data[1][i].total+" \n\
                                                </p> \n\
                                            </li> \n\
                                        </ul> \n\
                                    </div>";
            $("#productos").html(productos_insert);
        }
        
        $("#itemsTotal1").html(data[1].length);
        $("#itemsTotal2").html(data[1].length);
        
        $("#subtotal").html(data[0][0].subtotal);
        $("#iva").html(data[0][0].iva);
        $("#total").html(data[0][0].total);
        var fecha_ing = data[0][0].fecha.split("T");
        $("#fecha").html(fecha_ing[0]);
    });

    
});