$(document).ready(function() {
    event.preventDefault();
    
    var id_insert = localStorage.getItem("user_id");
    var id = window.location.search.substr(11);
    
    var data = {
        id : id_insert
    }
    
    $.ajax({ 
        url: "http://localhost:8081/usuario_secion",
        type: "POST",
        data: JSON.stringify(data),
        contentType:"application/json",
    }).then(function(data) {
        var persona = "<h3>";
        persona += "Bienvenído " +data.nombres+ "</h3>";
        $("#nombre").html(persona);
    });
    
    $.ajax({
        url: "http://localhost:8081/pedidos",
        type: "GET",
        // data: JSON.stringify(data),
        contentType:"application/json",
    }).then(function(data1) {
        var pedidos = "";
        
        for(var i = 0; i < data1[1].length; i++) {
            if(data1[1][i].pedido_id == id) {
                pedidos += "<div class='order-md-2 mb-4'> \n\
                               <ul class='list-group mb-3'> \n\
                                   <li class='list-group-item d-flex justify-content-between lh-condensed'> \n\
                                       <div> \n\
                                           <h6 class='my-0'> \n\
                                               Pedido id: " +data1[1][i].pedido_id+ " \n\
                                           </h6> \n\
                                           <pre></pre> \n\
                                           <h6> \n\
                                               Cantidad: " +data1[1][i].cantidad+ " \n\
                                           </h6> \n\
                                           <pre></pre> \n\
                                           <h6> \n\
                                               Precio Unitario: $" +data1[1][i].precio_unitario+ " \n\
                                           </h6> \n\
                                       </div> \n\
                                       <p> \n\
                                           Total: $" +data1[1][i].total+ " \n\
                                       </p> \n\
                                   </li> \n\
                               </ul> \n\
                           </div>";
            }
            
        }
        
        $("#pedidos").html(pedidos);
    });
    
});