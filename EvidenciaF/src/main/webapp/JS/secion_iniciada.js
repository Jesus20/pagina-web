$(document).ready(function() {
    event.preventDefault();
    
    var id_insert = localStorage.getItem("user_id");
    
    var data = {
        id : id_insert
    }
    
    $.ajax({ 
        url: "http://localhost:8081/usuario_secion",
        type: "POST",
        data: JSON.stringify(data),
        contentType:"application/json",
    }).then(function(data) {
        var persona = "<h3>";
        persona += "Bienvenído " +data.nombres+ "</h3>";
        $("#nombre").html(persona);
    });
    
    $.ajax({
        url: "http://localhost:8081/pedidos",
        type: "GET",
        // data: JSON.stringify(data),
        contentType:"application/json",
    }).then(function(data1) {
        var prueba = "";
        
        for(var i = 0; i < data1[0].length; i++) {
            prueba += "<div class='order-md-2 mb-4'> \n\
                           <ul class='list-group mb-3'> \n\
                                <a href='http://localhost:8080/EvidenciaF/pedidos.html?id_pedido=" +(i+1)+ "'> \n\
                                   <li class='list-group-item d-flex justify-content-between lh-condensed'> \n\
                                       <div> \n\
                                           <h6 class='my-0'> \n\
                                               Pedido " +(i+1)+ " \n\
                                           </h6> \n\
                                           <pre></pre> \n\
                                           <h6> \n\
                                               IVA: " +data1[0][i].iva+ " \n\
                                           </h6> \n\
                                           <pre></pre> \n\
                                           <h6> \n\
                                               Subtotal: $" +data1[0][i].subtotal+ " \n\
                                           </h6> \n\
                                       </div> \n\
                                       <p> \n\
                                           Total: $" +data1[0][i].total+ " \n\
                                       </p> \n\
                                   </li> \n\
                               </a>\n\
                           </ul> \n\
                       </div>";
        }
        
        $("#pedidos").html(prueba);
    });
    
});