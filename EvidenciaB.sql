CREATE DATABASE  IF NOT EXISTS `api` /*!40100 DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci */ /*!80016 DEFAULT ENCRYPTION='N' */;
USE `api`;
-- MySQL dump 10.13  Distrib 8.0.19, for Win64 (x86_64)
--
-- Host: localhost    Database: api
-- ------------------------------------------------------
-- Server version	8.0.19

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `categoria`
--

DROP TABLE IF EXISTS `categoria`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `categoria` (
  `id` int NOT NULL AUTO_INCREMENT,
  `nombre` varchar(15) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `categoria`
--

LOCK TABLES `categoria` WRITE;
/*!40000 ALTER TABLE `categoria` DISABLE KEYS */;
INSERT INTO `categoria` VALUES (1,'Café'),(2,'Grano'),(3,'Capsula'),(4,'Cafeteras'),(5,'Accesorios');
/*!40000 ALTER TABLE `categoria` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `pedido`
--

DROP TABLE IF EXISTS `pedido`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `pedido` (
  `id` int NOT NULL AUTO_INCREMENT,
  `fecha` date NOT NULL,
  `iva` float NOT NULL,
  `subtotal` float NOT NULL,
  `total` float NOT NULL,
  `usuario_id` int NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_pedido_usuario1_idx` (`usuario_id`),
  CONSTRAINT `fk_pedido_usuario1` FOREIGN KEY (`usuario_id`) REFERENCES `usuario` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `pedido`
--

LOCK TABLES `pedido` WRITE;
/*!40000 ALTER TABLE `pedido` DISABLE KEYS */;
INSERT INTO `pedido` VALUES (1,'2020-03-30',100,2000,2100,1);
/*!40000 ALTER TABLE `pedido` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `pedido_carro`
--

DROP TABLE IF EXISTS `pedido_carro`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `pedido_carro` (
  `id` int NOT NULL AUTO_INCREMENT,
  `fecha` date NOT NULL,
  `iva` float NOT NULL,
  `subtotal` float NOT NULL,
  `total` float NOT NULL,
  `usuario_id` int NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_pedido_usuario1_idx` (`usuario_id`),
  CONSTRAINT `fk_pedido_usuario10` FOREIGN KEY (`usuario_id`) REFERENCES `usuario` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `pedido_carro`
--

LOCK TABLES `pedido_carro` WRITE;
/*!40000 ALTER TABLE `pedido_carro` DISABLE KEYS */;
INSERT INTO `pedido_carro` VALUES (1,'2020-03-30',100,2000,2100,1);
/*!40000 ALTER TABLE `pedido_carro` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `pedido_producto`
--

DROP TABLE IF EXISTS `pedido_producto`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `pedido_producto` (
  `id` int NOT NULL AUTO_INCREMENT,
  `cantidad` int NOT NULL,
  `precio_unitario` float NOT NULL,
  `total` float NOT NULL,
  `pedido_id` int NOT NULL,
  `producto_id` int NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_pedido_producto_pedido1_idx` (`pedido_id`),
  KEY `fk_pedido_producto_producto1_idx` (`producto_id`),
  CONSTRAINT `fk_pedido_producto_pedido1` FOREIGN KEY (`pedido_id`) REFERENCES `pedido` (`id`),
  CONSTRAINT `fk_pedido_producto_producto1` FOREIGN KEY (`producto_id`) REFERENCES `producto` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `pedido_producto`
--

LOCK TABLES `pedido_producto` WRITE;
/*!40000 ALTER TABLE `pedido_producto` DISABLE KEYS */;
INSERT INTO `pedido_producto` VALUES (1,2,500,2000,1,1);
/*!40000 ALTER TABLE `pedido_producto` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `pedido_producto_carro`
--

DROP TABLE IF EXISTS `pedido_producto_carro`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `pedido_producto_carro` (
  `id` int NOT NULL AUTO_INCREMENT,
  `cantidad` int NOT NULL,
  `precio_unitario` float NOT NULL,
  `total` float NOT NULL,
  `producto_id` int NOT NULL,
  `pedido_carro_id` int NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_pedido_producto_producto1_idx` (`producto_id`),
  KEY `fk_pedido_producto_copy1_pedido_carro1_idx` (`pedido_carro_id`),
  CONSTRAINT `fk_pedido_producto_copy1_pedido_carro1` FOREIGN KEY (`pedido_carro_id`) REFERENCES `pedido_carro` (`id`),
  CONSTRAINT `fk_pedido_producto_producto10` FOREIGN KEY (`producto_id`) REFERENCES `producto` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=20 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `pedido_producto_carro`
--

LOCK TABLES `pedido_producto_carro` WRITE;
/*!40000 ALTER TABLE `pedido_producto_carro` DISABLE KEYS */;
INSERT INTO `pedido_producto_carro` VALUES (1,2,500,2000,1,1);
/*!40000 ALTER TABLE `pedido_producto_carro` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `producto`
--

DROP TABLE IF EXISTS `producto`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `producto` (
  `id` int NOT NULL AUTO_INCREMENT,
  `nombre` varchar(60) NOT NULL,
  `caracteristicas` varchar(600) NOT NULL,
  `marca` varchar(15) NOT NULL,
  `imagen` varchar(15) NOT NULL,
  `precio` float NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=19 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `producto`
--

LOCK TABLES `producto` WRITE;
/*!40000 ALTER TABLE `producto` DISABLE KEYS */;
INSERT INTO `producto` VALUES (1,'Starbucks Willow Blend','Hemos extraído con delicadeza los fieles sabores de esta mezcla de granos procedentes de Latinoamérica y África oriental, aplicando un exclusivo estilo de tostado más ligero a aquellos granos cuyos sabores delicados alcanzan la plenitud con menos tiempo de tostado.','Starbucks','producto1.jpg',1000),(2,'Starbucks Colombia','Dos mil metros, en vertical. Suena extremo, ya lo sabemos. Pero en las cumbres de los majestuosos Andes, en un paisaje escarpado de volcanes de actividad latente, es donde les gusta crecer a los mejores granos de café de Colombia','Starbucks','producto2.jpg',450),(3,'Starbucks Ethiopia','En Ethiopia, el café es mucho más que una bebida. La primera planta de café arábica creció aquí. Un antiguo proverbio etíope: \'Buna dabo Naw\' se traduce como: \'El café es nuestro pan\'.','Starbucks','producto3.jpg',583.1),(4,'Starbucks House Blend','Un sorbo e inmediatamente reconocerás las caracterizticas que hacen de este café un dsitintivo de Starbucks. Atención a la calidad de los más finos granos de café Latinoamericano.','Starbucks','producto4.jpg',250),(5,'Starbucks Sumatra','¿Por qué nos gusta tanto? Tiene un cuerpo denso y cremoso sin prácticamente ninguna acidez, lo que permite que el sabor y la intensidad del café permanezcan en el paladar. No te equivoques, es un café para beber a sorbos. En su mejor momento, son evidentes las notas herbáceas concentradas y su aroma terroso, marcas distintivas que esta bebida deliciosa nos ha dejado en el corazón.','Starbucks','producto5.jpg',400),(6,'The Original Donut Shop Coffee','The Original Donut Shop Coffee. Satisfactoriamente simple. Alegremente sencillo. Como debe ser el café. Una compañera consistentemente sabrosa y 100% cafedelicioso para todos los momentos inesperadamente maravillosos que hacen que la vida sea realmente grandiosa. Con cuerpo, audaz y sabroso, nuestro Regular es, simplemente, una taza llena de felicidad.','Donut Shop','producto6.jpg',361.43),(7,'Classic Roast Coffee','El café tostado clásico Folgers está hecho de frijoles Mountain Grown, los más ricos y aromáticos del mundo.','FOLGERS','producto7.jpg',361.43),(8,'Original Coffee','Nuestras capsulas KAHLÚA K-Cup originales están rostizadas con granos 100% Arábica elegidos por su calidad superior y características únicas, lo que garantiza un sabor rico y vivo.','KAHLÚA','producto8.jpg',373.1),(9,'Lake & Lodge Coffee','Animado y aromático, este café indonesio tostado oscuro rebosa de notable complejidad. Una mezcla rica y abundante de sabores terrosos y dulzura almibarada, con un acabado suave y ahumado.','GREEN MOUNTAIN','producto9.jpg',349.77),(10,'Cafe Domingo Coffee','No hay nada como un buen café para reunir a amigos y vecinos. Este asado mediano latinoamericano rinde homenaje a un viejo pasatiempo: quedarse en uno de nuestros cafés y beber una taza elaborada por expertos.','PEET\'S COFFEE','producto10.jpg',396.43),(11,'Keurig K155 OfficePRO Premier Brewing System','Características clave:\n     • Pantalla táctil LCD a todo color.\n     • Temperatura de preparación ajustable.\n     • Funciones programables como idioma y hora del reloj.\n     • Función de encendido / apagado automático.\n     • Tecnología Quiet Brew.\n     • Extra grande 90oz. Reserva de agua.\n     • Drena fácilmente para su transporte y almacenamiento.','KEURIG','producto11.jpg',5832.15),(12,'K-Duo Plus Single Serve & Carafe Coffee Maker','Dimensiones de la cafetera: \n     • Depósito de agua en la parte trasera: 14.19\'H x 7.68\'W x 15.88\'D.\n     • Altura con la manija abierta: 17.5\'H.\n     • Espacio libre para tazas de 7.25 con bandeja de goteo (se ajusta a una taza de viaje de 8.0 de alto sin bandeja de goteo).\n     • Cervecero: 10.97 libras.\n     • Jarra: 1.78 libras.\n     • 39 centimetros de longitud del cable.\n     • 120 voltios.','KEURIG','producto12.jpg',5366.41),(13,'Keurig K15 Coffee Maker','Características clave:\n     • Prepara café, té, chocolate caliente, especialidades y bebidas heladas.\n     • Prepara en menos de dos minutos.\n     • Tamaños de preparación de vainas K-Cup®: 6, 8, 10 oz.\n     • Tamaño compacto.\n     • controles de botón.\n     • Las luces indicadoras ayudan a guiarlo en cada preparación.\n     • Se apaga automáticamente después de 90 segundos de inactividad.\n     • Disponible en múltiples colores.\n     • La función de almacenamiento del cable ayuda con la portabilidad.\n     • Compatible con el filtro de café reutilizable My K-Cup.','KEURIG','producto13.jpg',2333.09),(14,'Keurig K-Compact Coffee Maker','Características clave:\n     • Prepara 6, 8 y 10 oz. tamaños.\n     • Nuestra cervecera extraíble de depósito más delgada.\n     • Depósito de agua trasero de 36 onzas.\n     • Se apaga automáticamente 2 horas después de la última infusión.\n     • Bandeja de goteo extraíble para tazas de viaje.\n     • Controles de botones táctiles simples.\n     • Recién hecho en el momento que lo quieres.\n     • Compatible con el filtro de café reutilizable My K-Cup.','KEURIG','producto14.jpg',2000),(15,'K-Mini Plus Single Serve Coffee Maker','Especificaciones:\n     • 12.1 de alto (16.8 con la manija abierta) x 4.5 de ancho x 11.3 de profundidad.\n     • Espacio libre para tazas de 6.0 con bandeja de goteo (se ajusta a una taza de viaje de 7.0 sin bandeja de goteo)\n     • 4.6 libras.\n     • 120V.','KEURIG','producto15.jpg',2333.09),(16,'My K-Cup® Filtro de café universal reutilizable','El filtro universal reutilizable Keurig My K-Cup es una manera simple y conveniente de preparar su café molido favorito con su cafetera Keurig. Ahora, además de cientos de deliciosas variedades de vainas K-Cup, puedes disfrutar de una variedad aún mayor de café. El filtro universal reutilizable Keurig My K-Cup es el único filtro de café reutilizable aprobado para su uso en todas las cafeteras Keurig K-Cup.','KEURIG','producto16.jpg',349.77),(17,'Taza de viaje con aislamiento Keurig de 12 oz','La taza de viaje con aislamiento tiene el tamaño ideal para 10 oz. K-Cup con mucho espacio para leche o crema, ¡además se adapta a TODOS los cerveceros Keurig, incluido el Keurig MINI Plus! Es fácil de transportar pero difícil de derramar, perfecto para donde sea que la vida te lleve.','KEURIG','producto17.jpg',233.1),(18,'K-Cup Pod Carousel','Muestre más de sus bebidas favoritas que nunca con el carrusel Keurig K-Cup Pod. La capacidad adicional le permite almacenar hasta 36 cápsulas K-Cup en una pantalla conveniente, para que sus bebidas favoritas estén siempre a mano. Ya sea que entretenga a los invitados ofreciendo una variedad de bebidas, o simplemente tratando de mantener limpia su cocina, es el accesorio de almacenamiento perfecto.','KEURIG','producto18.jpg',699.76);
/*!40000 ALTER TABLE `producto` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `producto_categoria`
--

DROP TABLE IF EXISTS `producto_categoria`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `producto_categoria` (
  `id` int NOT NULL AUTO_INCREMENT,
  `producto_id` int NOT NULL,
  `categoria_id` int NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_producto_categoria_producto_idx` (`producto_id`),
  KEY `fk_producto_categoria_categoria1_idx` (`categoria_id`),
  CONSTRAINT `fk_producto_categoria_categoria1` FOREIGN KEY (`categoria_id`) REFERENCES `categoria` (`id`),
  CONSTRAINT `fk_producto_categoria_producto` FOREIGN KEY (`producto_id`) REFERENCES `producto` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=57 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `producto_categoria`
--

LOCK TABLES `producto_categoria` WRITE;
/*!40000 ALTER TABLE `producto_categoria` DISABLE KEYS */;
INSERT INTO `producto_categoria` VALUES (1,1,1),(2,1,2),(3,2,1),(4,2,2),(5,3,1),(6,3,2),(7,4,1),(8,4,2),(9,5,1),(10,5,2),(11,6,1),(12,6,3),(13,7,1),(14,7,3),(15,8,1),(16,8,3),(17,9,1),(18,9,3),(19,10,1),(20,10,3),(21,11,4),(22,12,4),(23,13,4),(24,14,4),(25,15,4),(26,16,5),(27,17,5),(28,18,5),(29,1,1),(30,1,2),(31,2,1),(32,2,2),(33,3,1),(34,3,2),(35,4,1),(36,4,2),(37,5,1),(38,5,2),(39,6,1),(40,6,3),(41,7,1),(42,7,3),(43,8,1),(44,8,3),(45,9,1),(46,9,3),(47,10,1),(48,10,3),(49,11,4),(50,12,4),(51,13,4),(52,14,4),(53,15,4),(54,16,5),(55,17,5),(56,18,5);
/*!40000 ALTER TABLE `producto_categoria` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `usuario`
--

DROP TABLE IF EXISTS `usuario`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `usuario` (
  `id` int NOT NULL AUTO_INCREMENT,
  `nombres` varchar(45) NOT NULL,
  `apellidos` varchar(45) NOT NULL,
  `correo` varchar(25) NOT NULL,
  `contrasenia` varchar(15) NOT NULL,
  `telefono` int NOT NULL,
  `direccion1` varchar(45) DEFAULT NULL,
  `direccion2` varchar(45) DEFAULT NULL,
  `ciudad` varchar(15) DEFAULT NULL,
  `estado` varchar(15) DEFAULT NULL,
  `codigo_postal` int DEFAULT NULL,
  `tipo_tarjeta` varchar(20) DEFAULT NULL,
  `numero_tarjeta` bigint DEFAULT NULL,
  `fecha_expiracion` date DEFAULT NULL,
  `cvv` int DEFAULT NULL,
  `token` varchar(1) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `usuario`
--

LOCK TABLES `usuario` WRITE;
/*!40000 ALTER TABLE `usuario` DISABLE KEYS */;
INSERT INTO `usuario` VALUES (1,'Jesús Armando','Ayala Silguero','al02706110@tecmilenio.mx','contrasenia',216813,'prueba1','prueba2','México','Nuevo Leon',65186,'Tarjeta',35185135,'2020-03-30',8513,NULL);
/*!40000 ALTER TABLE `usuario` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2020-07-14 20:30:38
